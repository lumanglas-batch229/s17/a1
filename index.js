/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
// first function here:
function printUserInfo() {
	let yourName = prompt("What is your name? :");
	let yourAge = prompt("How old are you? :");
	let yourAddress = prompt("Where do you live? ");
	

	console.log("Hello, " + yourName);
	console.log("You are " + yourAge+ " years old");
	console.log("You live in " + yourAddress);
}

printUserInfo ();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function myFavMusic () {
	let music1 = "The Beatles";
	let music2 = "Metallica";
	let music3 = "The Eagles";
	let music4 = "Larcenciel";
	let music5 = "Eraserheads";
	console.log("1. " + music1);
	console.log("2. " + music2);
	console.log("3. " + music3);
	console.log("4. " + music4);
	console.log("5. " + music5);

}
myFavMusic();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function myFavMovies () {
	let movie1 = "The Godfather";
	let movie2 = "The Godfather, Part II";
	let movie3 = "Shawshank Redemption";
	let movie4 = "To kill A Mockingbird";
	let movie5 = "Psycho";

	console.log("1. " + movie1);
		console.log("Rotten Tomatoes Rating : 97%")
	console.log("2. " + movie2);
		console.log("Rotten Tomatoes Rating : 96%")
	console.log("3. " + movie3);
		console.log("Rotten Tomatoes Rating : 91%")
	console.log("4. " + movie4);
		console.log("Rotten Tomatoes Rating : 93%")
	console.log("5. " + movie5);
		console.log("Rotten Tomatoes Rating : 96%")

}
myFavMovies();


/*	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();

function printFriends() {
	alert("Hi! Please add the names of your friends.")
	//function printUsers(){
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: ")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	}

printFriends ();



//console.log(friend1);
//console.log(friend2);